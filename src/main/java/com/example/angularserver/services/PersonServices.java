package com.example.angularserver.services;

import com.example.angularserver.dao.PersonDao;
import com.example.angularserver.database.Person;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PersonServices {

    private PersonDao personDao = new PersonDao();

    public Person findById(int id) {
        return personDao.findById(id);
    }
    public List<Person> getAll() {
        return personDao.getAll();
    }


}
