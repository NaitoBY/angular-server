package com.example.angularserver.services;

import com.example.angularserver.dao.FacultyDao;
import com.example.angularserver.database.Faculty;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FacultyServices {

    private FacultyDao facultyDao = new FacultyDao();

    public Faculty getById(int id) {
        return facultyDao.getById(id);
    }
    public void remove(Faculty faculty) {
       facultyDao.remove(faculty);
    }

    public void saveFaculty(Faculty faculty) {
        facultyDao.save(faculty);
    }

    public void updateFaculty(Faculty faculty) {

        facultyDao.update(faculty);
    }
    public List<Faculty> getAll() {
        return facultyDao.getAll();
    }

    public Faculty getFacultyById(int id){
        return facultyDao.getFacyltyById(id);
    }

}
