package com.example.angularserver.controler;

import com.example.angularserver.database.Person;
import com.example.angularserver.services.PersonServices;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RequestMapping("/authors")
public class PersonController {

    private PersonServices personServices;

    public PersonController(PersonServices authorService) {
        this.personServices = authorService;
    }

    @GetMapping
    public List<Person> getPersons() {
        return personServices.getAll();
    }
}
