package com.example.angularserver.controler;

import com.example.angularserver.database.Faculty;
import com.example.angularserver.database.Person;
import com.example.angularserver.services.FacultyServices;
import com.example.angularserver.services.PersonServices;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RequestMapping("/faculty")
public class FacultyController {
    private FacultyServices facultyServices;
    private PersonServices personServices;

    public FacultyController(FacultyServices facultyServices, PersonServices personServices) {
        this.facultyServices = facultyServices;
        this.personServices = personServices;
    }

    @GetMapping
    public List<Faculty> getFaculty() {
        return facultyServices.getAll();
    }

    @GetMapping("/filter")
    public List<Faculty> filterFaculty(@RequestParam Optional<String> personId) {
        List<Faculty> filteredFaculty = facultyServices.getAll();
        String id = personId.orElse("null");
        if(!id.equals("null") && !id.equals("undefined")) {
            Person person = personServices.findById(Integer.parseInt(id));
            filteredFaculty.removeIf(faculty -> !faculty.getPersons().contains(person));
        }

        return filteredFaculty;
    }

    @GetMapping("/{id}")
    public Faculty getById(@PathVariable("id") Integer id) {
        return facultyServices.getFacultyById(id);
    }

    @PostMapping
    public void saveFaculty(@RequestBody Faculty faculty) {
        facultyServices.saveFaculty(faculty);
    }

    @PutMapping("/{id}")
    public void editFaculty(@PathVariable("id") Integer id, @RequestBody Faculty faculty) {
        facultyServices.updateFaculty(faculty);
    }

    @DeleteMapping("/{id}")
    public void deleteFaculty(@PathVariable("id") Integer id) {
        facultyServices.remove(facultyServices.getById(id));
    }
}
