package com.example.angularserver.dao;

import com.example.angularserver.database.Faculty;
import org.hibernate.Session;
import org.hibernate.Transaction;
import com.example.angularserver.utils.HibernateSessionFactoryUtil;
import java.util.List;

public class FacultyDao {
    private Session session;

    public Faculty getById(int id) {
        session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Faculty faculty = session.get(Faculty.class, id);
        session.close();
        return faculty;
    }

    public Faculty getFacyltyById(int id) {
        session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        return (Faculty) session.createQuery("from faculty b join fetch b.persons where b.id=:id").setParameter("id", id).list().get(0);
    }
    public void save(Faculty faculty) {
        session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.saveOrUpdate(faculty);
        transaction.commit();
        session.close();
    }
    public void remove(Faculty faculty) {
        setPersonWithSuchFacultyToNull(faculty);
        session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.remove(faculty);
        transaction.commit();
        session.close();
    }
    public void setPersonWithSuchFacultyToNull(Faculty faculty) {
        session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        faculty.setPersons(null);
        session.merge(faculty);
        transaction.commit();
        session.close();
    }

    public void update(Faculty faculty) {
        session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.update(faculty);
        tx1.commit();
        session.close();
    }

    public List<Faculty> getAll() {
        session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        return session.createQuery("from Faculty").list();
    }


}
